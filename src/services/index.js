import axios from "axios";

const API_EONET_PREFIX = "https://eonet.sci.gsfc.nasa.gov/api";
const VERSION = "v2.1";

const API_EVENTS = `${API_EONET_PREFIX}/${VERSION}/events`;
const API_CATEGORIES = `${API_EONET_PREFIX}/${VERSION}/categories`;

const GetEvents = async params => {
  try {
    const response = await axios.get(`${API_EVENTS}`, { params });
    return response.data;
  } catch (err) {
    return err;
  }
};

const GetCategories = async () => {
  try {
    const response = await axios.get(`${API_CATEGORIES}`);
    return response.data;
  } catch (err) {
    return err;
  }
};

const GetEventsByCategories = async (cateId, params) => {
  try {
    const response = await axios.get(`${API_CATEGORIES}/${cateId}`, { params });
    return response.data;
  } catch (err) {
    return err;
  }
};

export default {
  GetEvents,
  GetCategories,
  GetEventsByCategories
};
