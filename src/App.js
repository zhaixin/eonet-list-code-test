import React, { useState, useEffect } from "react";
import { Layout, Table, Divider, Tag, Select, Descriptions, Badge } from "antd";
import API from "./services";
import "./App.css";

const { Header, Content } = Layout;
const { Option } = Select;

const daysOptions = [
  { value: 1, text: "Today" },
  { value: 7, text: "Last Week" },
  { value: 30, text: "Last Month" },
  { value: 90, text: "Last 3 Month" },
  { value: 180, text: "Last 6 Month" },
  { value: 365, text: "Last Year" }
];

const columns = [
  {
    title: "Title",
    dataIndex: "title",
    key: "title"
  },
  {
    title: "Description",
    dataIndex: "description",
    key: "description"
  },
  {
    title: "Categories",
    dataIndex: "categories",
    key: "categories",
    render: categories => (
      <span>
        {categories.map(cate => {
          return (
            <Tag color={"green"} key={cate.id}>
              {cate.title.toUpperCase()}
            </Tag>
          );
        })}
      </span>
    )
  },
  {
    title: "Sources",
    key: "sources",
    dataIndex: "sources",
    render: sources => (
      <span>
        {sources.map((src, i) => {
          if (i === sources.length - 1) {
            return (
              <a key={i} href={src.url}>
                {src.id}
              </a>
            );
          }
          return (
            <>
              <a key={i} href={src.url}>
                {src.id}
              </a>
              <Divider type="vertical" />
            </>
          );
        })}
      </span>
    )
  },
  {
    title: "Status",
    key: "closed",
    render: (text, record) => {
      return record.closed ? <span>Closed at {new Date(record.closed).toDateString()}</span> : <span>OPEN</span>;
    }
  }
];

function EventCard(props) {
  const [event, setEvent] = useState(null);

  useEffect(() => {
    let unmounted = false;

    async function fetchEvent() {
      const res = await fetch(props.record.link);
      const event = await res.json();
      if (!unmounted && event) {
        setEvent(event);
      }
    }

    fetchEvent();
    return () => {
      unmounted = true;
    };
  }, [props.record]);

  return event ? (
    <Descriptions layout="vertical" size="small" bordered>
      <Descriptions.Item label="ID">{event.id}</Descriptions.Item>
      <Descriptions.Item label="Title">{event.title}</Descriptions.Item>
      <Descriptions.Item label="Link">{event.link}</Descriptions.Item>
      {event.description && (
        <Descriptions.Item label="Description" span={3}>
          {event.description}
        </Descriptions.Item>
      )}
      <Descriptions.Item label="Status" span={3}>
        {event.closed ? <Badge status="default" text="Closed" /> : <Badge status="processing" text="Open" />}
      </Descriptions.Item>
      {event.categories &&
        event.categories.map(cate => {
          return (
            <Descriptions.Item label={`Categories - ${cate.title}`} span={3}>
              {props.category.find(cat => cat.id === cate.id).description}
            </Descriptions.Item>
          );
        })}
      <Descriptions.Item label="Sources" span={3}>
        {event.sources.map(src => src.id).join()}
      </Descriptions.Item>
      {event.geometries &&
        event.geometries.map(geo => {
          return (
            <Descriptions.Item label={`Geometries - ${geo.type}`}>
              {`Date: ${new Date(geo.date).toLocaleString()}`} <br /> {`Coordinates: ${geo.coordinates.toString()}`}
            </Descriptions.Item>
          );
        })}
    </Descriptions>
  ) : null;
}

function App() {
  const [events, setEvents] = useState([]);
  const [status, setStatus] = useState(null);
  const [days, setDays] = useState(7);
  const [cateID, setCateId] = useState(null);
  const [category, setCate] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    let unmounted = false;

    async function fetchEvents() {
      const res = cateID
        ? await API.GetEventsByCategories(cateID, { status, days })
        : await API.GetEvents({ status, days });
      if (!unmounted && res.events) {
        setEvents(res.events);
        setLoading(false);
      }
    }

    setLoading(true);
    fetchEvents();
    return () => {
      unmounted = true;
    };
  }, [status, days, cateID]);

  useEffect(() => {
    let unmounted = false;

    async function fetchCates() {
      const res = await API.GetCategories();
      if (!unmounted && res.categories) {
        setCate(res.categories);
      }
    }

    fetchCates();
    return () => {
      unmounted = true;
    };
  }, []);

  return (
    <Layout>
      <Header
        style={{
          background: "#fff",
          padding: 0,
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center"
        }}
      >
        <Select
          showSearch
          allowClear
          style={{ width: 200 }}
          placeholder="Select a category"
          optionFilterProp="children"
          onChange={setCateId}
          filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
        >
          {category &&
            category.map(cate => {
              return (
                <Option key={cate.id} value={cate.id}>
                  {cate.title}
                </Option>
              );
            })}
        </Select>
        <Select defaultValue={7} style={{ width: 200 }} placeholder="Select Days" onChange={setDays}>
          {daysOptions.map(opt => {
            return (
              <Option key={opt.value} value={opt.value}>
                {opt.text}
              </Option>
            );
          })}
        </Select>
        <Select allowClear style={{ width: 200 }} placeholder="Select Status" onChange={setStatus}>
          <Option value={"open"}>OPEN</Option>
          <Option value={"closed"}>CLOSE</Option>
        </Select>
      </Header>
      <Content style={{ margin: "24px 16px 0" }}>
        <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
          <Table
            rowKey="id"
            columns={columns}
            dataSource={events}
            rowClassName={record => (record.closed ? "disabled" : "")}
            expandedRowRender={record => <EventCard record={record} category={category} />}
            loading={loading}
          />
        </div>
      </Content>
    </Layout>
  );
}

export default App;
